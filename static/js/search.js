(function (window, document) {
  "use strict";

  const search = (e) => {
    const results = window.searchIndex.search(e.target.value, {
      bool: "OR",
      expand: true,
    });

    const resEl = document.getElementById("searchResults");
    const noResultsEl = document.getElementById("noResultsFound");

    resEl.innerHTML = "";
    if (results) {
      noResultsEl.style.display = "none";
      results.map((r) => {
        const { id, title, chapnum} = r.doc;
        const el = document.createElement("li");
        resEl.appendChild(el);

        // const chapNumber = document.createElement("span"); 
        // chapNumber.classList.add("chap-number");
        // el.textContent = `chapter ${chapnum}`; 
        // el.appendChild(chapNumber)

        const h3 = document.createElement("h3");
        el.appendChild(h3);

        const a = document.createElement("a");
        a.setAttribute("href", id);
        a.innerHTML = `<span class="chapnum">chapter ${chapnum}.</span> ${title}`;
        h3.appendChild(a);
      });
    } else {
      noResultsEl.style.display = "block";
    }
  };

  fetch("/search-index.json").then((response) =>
    response.json().then((rawIndex) => {
      window.searchIndex = elasticlunr.Index.load(rawIndex);
      document.getElementById("searchField").addEventListener("input", search);
    })
  );
})(window, document);
