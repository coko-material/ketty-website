---
title: "Sign Up and Log In"
class: "component-body  chapter  "
order: 1
---

Sign up
-------

Anyone who wants to use Ketty must sign up before creating books or being added as a book collaborator. When you visit the Ketty site, you will be required to provide the following information to sign up:

*   Given name
    
*   Surname
    
*   Email
    
*   Password
    
*   Confirmation of password.
    

![](/images/5ffb8224ea99_medium.png)

Sign up page

Once these details have been entered and you click ‘Sign up’, you will receive a verification request via email, and will need to click the link in the email to verify your email address.

Once this verification has been done, you can log in to Ketty using those same credentials, and can then be added to others’ books or create your own.

Log in
------

Once signed up, you can log in with the email and password you used to sign up.

![](/images/52e899362e82_medium.png)

Login page

If you forgot your password, you can click ‘Forgot your password?’ and will receive an email to reset your password once you enter the associated email.

![](/images/600bc2992b53_medium.png)

Reset password page

Logout
------

At any point, you can log out by clicking on your username initials in the top right corner of the site and then clicking ‘Logout’.

![](/images/de2c222c4819_medium.png)

Logout from the top navigation bar