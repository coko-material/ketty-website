---
title: "Christoph Kepper (Germany)"
image: "christoph.jpg"
part : 6
role: Trade Publishing Advisor  
---

Christoph has over 30 years of experience working with the web, a passion he continues to pursue with enthusiasm. After an attempt in 2007 to launch a book business based on Wikipedia content, he developed a deep interest in the publishing industry. This led him to [Lulu Press](https://www.lulu.com/), where, since 2015, he has been dedicated to helping content creators achieve greater success.