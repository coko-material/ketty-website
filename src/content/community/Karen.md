---
title: "Karen Lauritsen (USA)"
image: "karen2021.jpg"
part : 4
role: Open Education Advisor
---

Karen is the Senior Director, Publishing, with the Open Education Network ([OEN](https://open.umn.edu/oen)). She works with the higher education community to create strategies, resources and human connections for people who develop open educational resources. Karen also leads efforts to grow shared publishing infrastructure, which includes an ongoing Ketty pilot. Committed to open knowledge sharing, Karen has worked with learning communities for more than 20 years.
