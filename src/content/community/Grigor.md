---
title: "Grigor Malo (Albania)"
image: "grigor.jpg"
part : 3
role: Ketty Lead Developer
---

Grigor is a javascript developer with several years of experience. After succesfully leading the development of The Assessment Builder (a Coko platform built for the HHMI) he now leads the Ketty team. He has a keen interest in web accessibility. In his free time, Grigor likes to study mathematics and indulge in the wonders of philosophy.