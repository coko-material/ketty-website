---
title: "Dione Mentis (South Africa)"
image: "dione.jpg"
part : 2
role: Ketty Architect and Project Lead
---

Dione is Coko's COO and Senior Architect. She has extensive experience in platform design, project management, and strategic development within the open-source community. She brings her experience in coordinating large-scale digital projects and strong background in educational publishing to the Ketty community.  