---
title: "Open Source"
image: "ketida-open-source-2.svg"
part: 4 
---

Streamlining book publishing requires developing scalable, standards-based technology solutions. Ketty turned to the Coko Foundation to build a comprehensive solution for the entire publishing community.
